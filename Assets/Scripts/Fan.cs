﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour {

    Rigidbody rb;
    AudioSource Audio;
    public float rotationVelocity;

    public GameObject Light;

	// Use this for initialization
	void Start () {
        Audio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 100;
	}

    private void FixedUpdate()
    {
        rb.angularVelocity = new Vector3(0, rotationVelocity, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball") {
            Light.SetActive(true);
            Audio.Play();
            Invoke("turnOffLight", 0.25f);
        }
    }

    void turnOffLight()
    {
        Light.SetActive(false);
    }
}
