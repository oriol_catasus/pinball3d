﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    Rigidbody rb;
    AudioSource Audio;

    [Header("Vars to define obj")]
    public float forceBumper;
    public float limitSpeed;

    [Header("Vars to debugg & control")]
    public int num;

    private void Awake()
    {
        Audio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        rb.maxDepenetrationVelocity = 0;
    }

    private void FixedUpdate()
    {
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, limitSpeed);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Barrier"))
        {
            other.isTrigger = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            GameController.Instance.putBallToCanon(num);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Audio.Play();
        if (collision.gameObject.tag == "Bumper")
        {
            rb.AddForce(Vector3.Reflect(rb.velocity, collision.contacts[0].normal).normalized * forceBumper);   //This add force on bounce
        }
    }
}
