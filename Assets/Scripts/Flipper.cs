﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {

    public enum Side
    {
        left, right
    };
    Rigidbody rb;
    AudioSource Audio;

    [Header("Variables for define Flipper")]
    public float gradesToRotate;
    public float forceRaiseFlipper;
    public float forceFallFlipper;
    public float maxTimeUp;
    public Side side;

    [Header("Variables for control & debugg")]
    public float totalAnglesRotated;
    public float angleTarget;
    public float prevAngleY;
    public float maxRotationUpdate;
    public float prevTotalAnglesRotated;
    public bool raising, falling;
    public float initRotationY;

    

    private void Awake()
    {
        Audio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = transform.GetChild(0).localPosition;  //Here I define the center of mass of Rigidbody
        rb.maxAngularVelocity = 1000;
        initRotationY = transform.localEulerAngles.y;
        prevAngleY = initRotationY;
        calcAngleTarget();
    }

    void calcAngleTarget()  //Here I calculate the angle when the flipper will stop
    {
        if (side == Side.left)
        {
            angleTarget = initRotationY - gradesToRotate;
        }
        else
        {
            angleTarget = initRotationY + gradesToRotate;
        }
        if (angleTarget < 0)
        {
            angleTarget += 360;
        }
        else if (angleTarget >= 360)
        {
            angleTarget -= 360;
        }
    }
    
    void Update()
    {
        if ((((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) && side == Side.right) || ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) && side == Side.left)) && !raising)
        {
            raising = true;
            Audio.Play();
            rb.isKinematic = false;
            Invoke("startMoveDown", maxTimeUp);
        }
        else if (((Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D)) && side == Side.right) || ((Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A)) && side == Side.left))
        {
            CancelInvoke("startMoveDown");
            startMoveDown();
        }
    }

    private void FixedUpdate()
    {
        if (raising)
        {
            flipperRaising();
        }
        else if (falling)
        {
            flipperFalling();
        }
    }

    void flipperRaising()
    {
        if (totalAnglesRotated < gradesToRotate)
        {
            if (side == Side.left)
            {
                //rb.angularVelocity = new Vector3(0, rb.angularVelocity.y / 2 - forceRaiseFlipper, 0);
                rb.angularVelocity = new Vector3(0, -forceRaiseFlipper, 0);
                if (prevAngleY < 180 && transform.localEulerAngles.y > 180)
                {
                    prevAngleY += 360;
                }
            }
            else
            {
                rb.angularVelocity = new Vector3(0, rb.angularVelocity.y / 2 + forceRaiseFlipper, 0);
                rb.angularVelocity = new Vector3(0, forceRaiseFlipper, 0);
                if (prevAngleY > 180 && transform.localEulerAngles.y < 180)
                {
                    prevAngleY -= 360;
                }
            }
            totalAnglesRotated += Mathf.Abs(transform.localEulerAngles.y - prevAngleY);
            prevAngleY = transform.localEulerAngles.y;
            angleCorrection();
            
        }
    }

    //Here I calc if I need an angle correction
    void angleCorrection()
    {
        
        if (totalAnglesRotated + maxRotationUpdate > gradesToRotate)
        {
            rb.angularVelocity = Vector3.zero;
            transform.Rotate(new Vector3(0, angleTarget - transform.localEulerAngles.y));   //Here I put the flipper in the exact angle that is defined in gradesToRotate var
            totalAnglesRotated = gradesToRotate;
            maxRotationUpdate = 0;
            prevAngleY = angleTarget;
        }
        else if (totalAnglesRotated - prevTotalAnglesRotated > maxRotationUpdate)
        {
            maxRotationUpdate = totalAnglesRotated - prevTotalAnglesRotated;
        }
        prevTotalAnglesRotated = totalAnglesRotated;
    }

    void flipperFalling()
    {
        if (side == Side.left)
        {
            rb.angularVelocity = new Vector3(0, forceFallFlipper, 0);
            if (transform.localEulerAngles.y < 180 && prevAngleY > 180)
            {
                prevAngleY -= 360;
            }
        }
        else
        {
            rb.angularVelocity = new Vector3(0, -forceFallFlipper, 0);
            if (transform.localEulerAngles.y > 180 && prevAngleY < 180)
            {
                prevAngleY += 360;
            }
        }
        totalAnglesRotated -= Mathf.Abs(prevAngleY - transform.localEulerAngles.y);
        prevAngleY = transform.localEulerAngles.y;
        if (totalAnglesRotated - maxRotationUpdate <= 0)
        {
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = true;
            transform.localRotation = Quaternion.Euler(new Vector3(transform.localEulerAngles.x, initRotationY, transform.localEulerAngles.z)); //Here I put the flipper angle with his initial values
            prevAngleY = initRotationY;
            totalAnglesRotated = 0;
            maxRotationUpdate = 0;
            falling = false;
        }
        else if (prevTotalAnglesRotated - totalAnglesRotated > maxRotationUpdate)
        {
            maxRotationUpdate = prevTotalAnglesRotated - totalAnglesRotated;
        }
        prevTotalAnglesRotated = totalAnglesRotated;
    }

    void startMoveDown()
    {
        if (raising)
        {
            raising = false;
            falling = true;
        }

    }
}