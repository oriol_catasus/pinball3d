﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Cam : MonoBehaviour {

    [Header("Vars to define obj")]
    public float speedRotationArround;
    public float timeToGamePos;

    [Header("Vars to drag")]
    public Transform pinball;
    public Transform gamePos;
    public GameObject pressAnyKey;

    [Header("Vars to debug & control")]
    public bool initTravelToGamePos;
    public float startTime;
    public Vector3 initPos;
    public Quaternion initRotation;

	// Update is called once per frame
	void FixedUpdate () {
		if (GameController.Instance.state == GameController.State.PressAnyKey)
        {
            transform.RotateAround(pinball.position, Vector3.up, speedRotationArround);
        } else if (transform.position != gamePos.position || transform.rotation != gamePos.rotation)
        {
            if (!initTravelToGamePos)
            {
                init();
            }
            transform.position = Vector3.Lerp(initPos, gamePos.position, (Time.time - startTime) / timeToGamePos);
            transform.rotation = Quaternion.Lerp(initRotation, gamePos.rotation, (Time.time - startTime) / timeToGamePos);
            if (Mathf.Approximately((Time.time - startTime), timeToGamePos))
            {
                GameController.Instance.state = GameController.State.InsertCoin;
                GameController.Instance.initTextsCoin();
                Destroy(gamePos.gameObject);
                Destroy(this);
            }
        }
	}

    void init()
    {
        startTime = Time.time;
        initPos = transform.position;
        initRotation = transform.rotation;
        initTravelToGamePos = true;
        Destroy(pressAnyKey);
    }

}
