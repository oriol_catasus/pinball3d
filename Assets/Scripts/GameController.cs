﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;

    AudioSource Audio;

    public enum State
    {
        PressAnyKey, WaitingCam, InsertCoin, WaitingCanon, Playing
    }

    [Header("Vars to define GameController")]
    public float timeForPoints;
    public int pointsForTime;
    public float timeResetTargets;
    public string insertCoinText;
    public string[] textString;
    public State state;

    [Header("Vars to drag")]
    public GameObject[] canons;
    public GameObject[] targets;
    public GameObject[] balls;
    public GameObject menuPause;
    public Text[] texts;
    public AudioClip[] sound;

    [Header("Vars for control and debug")]
    public int numGames;
    public int game;
    public int firstCanon;
    public int score;
    public int highscore;
    public int[] timeGame = new int[2];
    public int numTargetsDown;
    public float lastTimeForPoints;
    

	// Use this for initialization
	void Awake () {
        Instance = this;
        Audio = GetComponent<AudioSource>();
        Random.InitState((int)System.DateTime.Now.Ticks);
        
    }

    //This function initialize balls
    void initBalls()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            balls[i] = Instantiate(balls[i]);
            balls[i].SetActive(false);
            balls[i].GetComponent<Ball>().num = i;
        }
    }

    //This function initialize texts to insert coin state
    public void initTextsCoin()
    {
        highscore = PlayerPrefs.GetInt("highscore");
        texts[0].text = textArcadeMode(0, highscore);
        texts[1].text = textArcadeMode(1, PlayerPrefs.GetInt("lastScore"));
        texts[2].text = insertCoinText;
        texts[3].text = timeArcadeMode(0, 0);
        texts[4].text = textArcadeMode(4, game)+"/3";
    }

    //This function initialize texts of the game
    public void initTextsGame()
    {
        
        texts[4].text = textArcadeMode(4, game)+"/3";
        addScore(0);
    }

    //This function convert the text to a text with "arcade mode" (HIGHSCORE: 000001256)
    string textArcadeMode(int index, int numbers)
    {
        char[] newText = textString[index].ToCharArray();
        string numbersToString = numbers.ToString();
        int numbersIndex = 0;
        for (int i = newText.Length - numbersToString.Length; i < newText.Length; i++)
        {
            newText[i] = numbersToString[numbersIndex];
            numbersIndex++;
        }
        return new string(newText);
    }

    //This function converts the time to a "arcade mode" or digital time mode (TIME: 00:00)
    string timeArcadeMode(int minutes, int seconds)
    {
        string[] timeString = new string[2];
        timeString[0] = subTimeArcadeMode(minutes);
        timeString[1] = subTimeArcadeMode(seconds);
        string time = textString[3] + timeString[0] + ":" + timeString[1];
        return new string(time.ToCharArray());

    }

    //This function returns the time passed by parameter into a string
    string subTimeArcadeMode(int time)
    {
        string subTime;
        string newString = time.ToString();
        if (newString.Length < 2)
        {
            subTime = "0" + newString;
        }
        else
        {
            subTime = newString;
        }
        return subTime;
    }

    public void Update()
    {
        if (state == State.PressAnyKey && Input.anyKeyDown)
        {
            Audio.clip = sound[4];
            Audio.Play();
            initBalls();
            state = State.WaitingCam;
        } else if (state == State.InsertCoin && Input.GetKeyDown(KeyCode.Return))
        {
            Audio.clip = sound[1];
            Audio.Play();
            initTextsGame();
            numGames = 0;
            balls[0].SetActive(true);
            putBallToCanon(0);
            state = State.WaitingCanon;
            StartCoroutine(countTimePoints());
            StartCoroutine(timeCounter());
        } else if (state != State.PressAnyKey && (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)))
        {
            if (Time.timeScale == 1)
            {
                setPause(true);
            } else if (Time.timeScale == 0)
            {
                setPause(false);
            }
        }
    }

    //Use this to put or quit pause
    public void setPause(bool pause)
    {
        if (pause)
        {
            menuPause.SetActive(true);
            Time.timeScale = 0;
        } else
        {
            menuPause.SetActive(false);
            Time.timeScale = 1;
        }
    }

    IEnumerator countTimePoints()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeForPoints);
            if (state == State.Playing)
            {
                if (!areAllBallsActive())
                {
                    addScore(pointsForTime);
                } else
                {
                    addScore(pointsForTime * 2);
                }
                
            }
        }
    }

    IEnumerator timeCounter()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (state == State.Playing)
            {
                if (timeGame[1] == 59)
                {
                    timeGame[0]++;
                    timeGame[1] = 0;
                } else
                {
                    timeGame[1]++;
                }
                texts[3].text = timeArcadeMode(timeGame[0], timeGame[1]);
            }
        }
    }

    public void putBallToCanon(int index)
    {
        numGames++;
        if (!areAllBallsActive())
        {
            Audio.clip = sound[2];
            Audio.Play();
            state = State.WaitingCanon;
            Invoke("raiseTarget", 0.25f);
            game++;
            texts[4].text = textArcadeMode(4, game)+"/3";
        } else
        {
            balls[index].SetActive(false);
        }
        if (numGames == 1)
        {
            firstCanon = Random.Range(0, 2);
            balls[index].transform.position = new Vector3(canons[firstCanon].transform.position.x - 0.35f, balls[index].transform.position.y, canons[firstCanon].transform.position.z);
            setBallToCanon(firstCanon, balls[index].GetComponent<Rigidbody>());
        }
        else if (numGames == 2)
        {
            if (firstCanon == 1)
            {
                balls[index].transform.position = new Vector3(canons[firstCanon - 1].transform.position.x - 0.35f, balls[index].transform.position.y, canons[firstCanon - 1].transform.position.z);
                setBallToCanon(firstCanon - 1, balls[index].GetComponent<Rigidbody>());
            } else
            {
                balls[index].transform.position = new Vector3(canons[firstCanon + 1].transform.position.x - 0.35f, balls[index].transform.position.y, canons[firstCanon + 1].transform.position.z);
                setBallToCanon(firstCanon + 1, balls[index].GetComponent<Rigidbody>());
            }
        } else if (numGames == 3)
        {
            balls[index].transform.position = new Vector3(canons[0].transform.position.x - 0.35f, balls[index].transform.transform.position.y, canons[0].transform.position.z);
            activeSecondaryBall(1, balls[index].transform.position.y);
            setBallToCanon();
        } else if (numGames >= 5)
        {
            resetGame();
        }
    }

    //Call this function to know if all balls are active
    bool areAllBallsActive()
    {
        for (int i = 0; i < balls.Length; i++)
        {
            if (!balls[i].activeInHierarchy)
            {
                return false;
            }
        }
        return true;
    }

    //Use this when game finished for reset game
    void resetGame() {
        Audio.clip = sound[3];
        Audio.Play();
        game = 0;
        PlayerPrefs.SetInt("lastScore", score);
        StopCoroutine(countTimePoints());
        StopCoroutine(timeCounter());
        for (int i = 0; i < balls.Length; i++)
        { 
            balls[i].SetActive(false);
            timeGame[i] = 0;
        }
        score = 0;
        state = State.InsertCoin;
        initTextsCoin();
    }

    //Use this function to active the secondary ball
    void activeSecondaryBall(int canonIndex, float posY)
    {
        Vector3 secondBallPos = new Vector3(canons[canonIndex].transform.position.x - 0.35f, posY, canons[canonIndex].transform.position.z);
        balls[1].transform.position = secondBallPos;
        balls[1].SetActive(true);
    }

    //This function put the ball passed by param. into de cannon whose number is passed by param.
    void setBallToCanon(int numCanon, Rigidbody ball)
    {
        Canon canon = canons[numCanon].GetComponent<Canon>();
        canon.ballHere = true;
        canon.ball = ball;
    }

    //This function put the 2 balls into the 2 canons.
    void setBallToCanon()
    {
        Canon canon1 = canons[0].GetComponent<Canon>();
        Canon canon2 = canons[1].GetComponent<Canon>();
        canon1.ballHere = true;
        canon2.ballHere = true;
        canon1.ball = balls[0].GetComponent<Rigidbody>();
        canon2.ball = balls[1].GetComponent<Rigidbody>();
    }

    //This function is called when one target is down
    public void setTargetDown()
    {
        numTargetsDown++;
        if (numTargetsDown == targets.Length)
        {
            Audio.clip = sound[0];
            Audio.Play();
            addScore((int)Mathf.Round(score * targets.Length));
            Invoke("raiseTarget", timeResetTargets);
        }
    }

    //Call this function to raise targets
    public void raiseTarget()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            Audio.clip = sound[5];
            Audio.Play();
            targets[i].SetActive(true);
            targets[i].GetComponent<Target>().move = Target.state.moveUp;
        }
        numTargetsDown = 0;
    }

    //Call this method to add score
    public void addScore(int points)
    {
        score += points;
        texts[2].text = textArcadeMode(2, score);
        if (score > highscore)
        {
            highscore = score;
            PlayerPrefs.SetInt("highscore", score);
            texts[0].text = textArcadeMode(0, highscore);
        }
    }

    //This function is called with the exit button
    public void exitButton()
    {
        Application.Quit();
    }
}