﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

    public int points;
    public AudioSource sound;

    private void Awake()
    {
        sound = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            GameController.Instance.addScore(points);
            transform.GetChild(0).gameObject.SetActive(true);
            sound.Play();
            Invoke("turnOffLight", 0.25f);
        }
    }

    void turnOffLight()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
