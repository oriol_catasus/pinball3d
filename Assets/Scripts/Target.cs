﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public enum state
    {
        moveDown, moveUp, noMove
    };
    AudioSource Audio;

    [Header("Vars to define target")]
    public float speedDown;
    public float speedUp;
    public int scorePoints;

    [Header("Vars to drag")]
    public Transform posDown;
    public GameObject Light;

    [Header("Vars to debugg & control")]
    public float initY;
    public state move;

	// Use this for initialization
	void Awake () {
        initY = transform.position.y;
        Audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (move == state.moveDown)
        {
            if (transform.position.y > posDown.position.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - speedDown, transform.position.z);
            } else
            {
                move = state.noMove;
                GameController.Instance.addScore(scorePoints);
                GameController.Instance.setTargetDown();
                gameObject.SetActive(false);
            }
        } else if (move == state.moveUp)
        {
            if (transform.position.y < initY)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + speedUp, transform.position.z);
            } else
            {
                move = state.noMove;
            }
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            move = state.moveDown;
            Light.SetActive(true);
            Audio.Play();
            Invoke("turnOffLight", 0.25f);
        }
    }

    void turnOffLight()
    {
        Light.SetActive(false);
    }
}
