﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour {

    [Header("Vars to define Canon")]
    public float maxCompresion;
    public float velCompresion;
    public float forceToBall;

    [Header("Vars to drag")]
    public Collider Barrier;
    public GameObject Light;

    [Header("Vars to control & debug")]
    public bool compresion, ballHere;
    public float amountCompresion;
    public Rigidbody ball;
	
	// Update is called once per frame
	void Update () {
        if (!ballHere)
        {
            return;
        } else if (!Light.activeInHierarchy)
        {
            Light.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            compresion = true;
        } else if (Input.GetKeyUp(KeyCode.Space))
        {
            compresion = false;
        }
	}

    private void FixedUpdate()
    {
        if (!ballHere)
        {
            return;
        }
        if (compresion && amountCompresion < maxCompresion)
        {
            transform.position = new Vector3(transform.position.x + velCompresion, transform.position.y, transform.position.z);
            amountCompresion += velCompresion;
        } else if (!compresion && amountCompresion >= velCompresion)
        {
            Barrier.isTrigger = true;
            ball.AddForce(-forceToBall - (amountCompresion * 100 / 5), 0, 0, ForceMode.Impulse);
            ballHere = false;
            ball = null;
            compresion = false;
            amountCompresion = 0;
            GameController.Instance.state = GameController.State.Playing;
            GameController.Instance.lastTimeForPoints = Time.time;
            Light.SetActive(false);
        }
    }
}